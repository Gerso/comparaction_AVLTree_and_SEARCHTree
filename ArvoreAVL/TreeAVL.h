/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TreeAVL.h
 * Author: gerson
 *
 * Created on 26 de Maio de 2018, 19:02
 */

#ifndef TREEAVL_H
#define TREEAVL_H

#include "Node.h"

class TreeAVL {
public:
    
    TreeAVL();
    virtual ~TreeAVL();

    void AVLifi(int vector[],int tam);
    
    void Clear();
    
    /**
     * call private method insert
     * @param key
     */
    void Insert(int key);

    /**
     * Search if the key exists. If there is a return address, otherwise return null
     * @param key
     * @return pointer to node
     */
    Node* Search(int key);
    
    /**
     * calls private method remove
     * @param key to be deleted 
     */
    bool Remove(int key);

    /**
     * calls the private method to go through the tree in preorder
     */
    void PrintAVLTree();
    
    /**
     * returns the number of values entered
     * @return count
     */
    int Size();
    
private:
    // element counter
    int count;
    Node* root;
    

    /**
     * method to insert an element in the tree
     * @param key: new element
     * @param &pA: root address
     * @param &h : flag to see if the height has been changed
     */
    void InsertAVL(int key, Node* &pA, bool &h);

    /**
     * Private method that removes an element from the AVL tree
     * @param key
     * @param &p
     * @param &h
     */
    bool RemoveAVL(int key, Node* &p, bool &h);
    
    void DelMin(Node* &q, Node* &r, bool &h);
    
    void RotateLL(Node* &pA, Node* &pB);

    void RotateLR(Node* &pA, Node* &pB, Node* &pC);

    void RotateRR(Node* &pA, Node* &pB);

    void RotateRL(Node* &pA, Node* &pB, Node* &pC);

    void BalanceL(Node* &pA, bool &h);

    void BalanceR(Node* &pA, bool &h);

    /**
     * method to traverse the tree in preOrder
     * @param node
     */
    void PreOrderAVL(Node * node);
};

#endif /* TREEAVL_H */

